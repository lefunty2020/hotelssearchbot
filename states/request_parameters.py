from datetime import datetime
from config_data import config
from database.sqlite import Options
from utils.base_functions import singleton
from telebot.handler_backends import State, StatesGroup
from lang.labels import get_message
from typing import Any


class RequestParametersState(StatesGroup):
    """
    Описатель возможных состояний бота:
    start
    date_enter
    prices
    city
    citydetail
    hotelscount
    distance
    showphotos
    photoscount
    completed
    """
    start = State()
    date_enter = State()
    prices = State()
    city = State()
    citydetail = State()
    hotelscount = State()
    distance = State()
    showphotos = State()
    photoscount = State()
    distance = State()
    confirmation = State()
    completed = State()


class UserParameters:
    """
    Структура пользовательских параметров
    """
    LANGUAGE = 'language'
    LANGUAGE_API = 'language_api'
    CURRENCY = 'currency'
    DATETIME_FORMAT = 'datetime_format'
    DATE_FORMAT = 'date_format'
    TIME_FORMAT = 'time_format'

    COMMAND = 'command'
    BEGIN_DATE = 'begin_date'
    END_DATE = 'end_date'
    CITY_ID = 'city_id'
    CITY_NAME = 'city_name'
    HOTELSCOUNT = 'hotelscount'
    SHOWPHOTOS = 'showphotos'
    PHOTOSCOUNT = 'photoscount'
    MIN_PRICE = 'min_price'
    MAX_PRICE = 'max_price'
    MIN_DISTANCE = 'min_distance'
    MAX_DISTANCE = 'max_distance'

    config_loaded = False
    language = config.DEFAULT_LANGUAGE
    language_api = config.DEFAULT_LANGUAGE_API
    datetime_format = config.DEFAULT_DATETIME_FORMAT
    date_format = config.DEFAULT_DATE_FORMAT
    time_format = config.DEFAULT_TIME_FORMAT
    currency = config.DEFAULT_CURRENCY

    command: str | None = None
    begin_date: datetime | None = None
    end_date: datetime | None = None
    city_id: int | str | None = None
    city_name: str | None = None
    hotelscount: int | None = None
    showphotos: bool | None = None
    photoscount: int | None = None
    min_price: int | None = None
    max_price: int | None = None
    min_distance: float = 0
    max_distance: float = 1000

    max_price_limit: int = 1000000
    max_distance_limit: float = 1000

    # Словарь для временного сохранения данных между запросами
    temp_data = dict()


@singleton
class ParametersManager:
    """
    Класс для манипуляций с пользовательскими параметрами
    """

    params_in_db = [
        UserParameters.LANGUAGE,
        UserParameters.LANGUAGE_API,
        UserParameters.DATETIME_FORMAT,
        UserParameters.DATE_FORMAT,
        UserParameters.TIME_FORMAT,
        UserParameters.CURRENCY
    ]

    def __init__(self):
        self.params = dict()

    @staticmethod
    def get_options_from_db(user_id) -> dict:
        """
        Получить значение всех сохраненных пользовательских парамеров из базы данных
        """
        result = dict()
        query = Options.select().where(Options.user_id == str(user_id))
        for option in query:
            result[option.option_id] = option.option_value

        return result

    @staticmethod
    def save_option_to_db(user_id, option_id, option_value):
        """
        Сохранить значение параметра в базу данных
        """
        query = Options.delete().where((Options.user_id == str(user_id)) & (Options.option_id == option_id))
        query.execute()

        option = Options(user_id=str(user_id), option_id=option_id)
        option.option_value = option_value
        option.save()

    @staticmethod
    def get_option_from_db(user_id, option_id, default_value) -> Any:
        """
        Получить значение параметра из базы данных
        """
        result = default_value
        query = Options.select().where((Options.user_id == user_id) & (Options.user_id == option_id))
        for value in query:
            result = value.option_value

        return result

    def get_parameters(self, user_id) -> UserParameters:
        """
        Пробуем загрузить параметры пользователя из базы данных
        если не найдем в базе данных, то подставим по умолчанию
        """
        userdata = self.params.get(user_id, UserParameters())

        if not userdata.config_loaded:
            options = self.get_options_from_db(user_id)

            userdata.language = options.get(UserParameters.LANGUAGE, config.DEFAULT_LANGUAGE)
            userdata.language_api = options.get(UserParameters.LANGUAGE_API, config.DEFAULT_LANGUAGE_API)
            userdata.currency = options.get(UserParameters.CURRENCY, config.DEFAULT_CURRENCY)
            userdata.datetime_format = options.get(UserParameters.DATETIME_FORMAT, config.DEFAULT_DATETIME_FORMAT)
            userdata.date_format = options.get(UserParameters.DATE_FORMAT, config.DEFAULT_DATE_FORMAT)
            userdata.time_format = options.get(UserParameters.TIME_FORMAT, config.DEFAULT_TIME_FORMAT)
            userdata.config_loaded = True

        return userdata

    def get_parameters_str(self, user_id: str | int) -> str:
        """
        Сформировать текстовое сообщение с текущими параметрами
        """
        userdata = self.get_parameters(user_id=user_id)
        message = f"{get_message('OPTIONS_CURRENT', lang=userdata.language)}\n" \
                  f"{get_message('LABEL_LANGUAGE', lang=userdata.language)} <b>{userdata.language}</b>\n" \
                  f"{get_message('LABEL_DATE_FORMAT', lang=userdata.language)}\n" \
                  f"{get_message('LABEL_CURRENCY', lang=userdata.language)} <b>{userdata.currency}</b>"

        return message

    def set_parameters(self, user_id, userdata:UserParameters) -> None:
        """
        Сохраняем параметры пользователя в память бота,
        а также сразу пишем их в базу данных
        """
        self.params[user_id] = userdata

        self.save_option_to_db(user_id, UserParameters.LANGUAGE, userdata.language)
        self.save_option_to_db(user_id, UserParameters.LANGUAGE_API, userdata.language_api)
        self.save_option_to_db(user_id, UserParameters.CURRENCY, userdata.currency)
        self.save_option_to_db(user_id, UserParameters.DATETIME_FORMAT, userdata.datetime_format)
        self.save_option_to_db(user_id, UserParameters.DATE_FORMAT, userdata.date_format)
        self.save_option_to_db(user_id, UserParameters.TIME_FORMAT, userdata.time_format)

    def set_parameter(self, user_id, key, value) -> None:
        """
        Установить значение параметра, при необходимости записываем его в базу данных
        """
        if not user_id in self.params.keys():
            self.params[user_id] = UserParameters()

        self.params[user_id].__setattr__(key, value)

        if key in self.params_in_db:
            self.save_option_to_db(user_id, key, value)

    def set_some_parameters(self, user_id, params:dict) -> None:
        """
        Установить значение выборочных параметров
        """
        for key, value in params.items():
            self.set_parameter(user_id, key, value)
