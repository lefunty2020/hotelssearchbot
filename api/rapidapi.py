import requests
from requests.exceptions import ConnectTimeout
import re
import json
from typing import Any
from utils.base_functions import singleton
from states.request_parameters import UserParameters
from loguru import logger


@singleton
class RapidApi:
    """
    Реализация запросов к сервису поиска отелей
    """

    api_key = ""
    base_host = "hotels4.p.rapidapi.com"
    base_url = "https://hotels4.p.rapidapi.com/"

    entry_search = "locations/v2/search"
    entry_detail = "properties/list"
    entry_photos = "properties/get-hotel-photos"

    def get_node(self, node: dict | None, order: list) -> Any | None:
        """
        Получить узел из переданной иерархии в соответствии с порядком

        :param node:   Переданная структура данных
        :param order:  Порядок вложенности узлов списком
        :return:       Полученный из структуры узел
        """
        if node is not None:
            for step in order:
                node = node.get(step, None)
                if node is None:
                    return None

        return node

    def request(self, url: str, params: dict) -> str | None:
        """
        Выполнение запроса к веб-сервису

        :param url:     адрес веб-сервиса
        :param params:  передаваемые параметры
        :return:        возвращамый текст
        """
        headers = {"X-RapidAPI-Host": self.base_host, "X-RapidAPI-Key": self.api_key}

        response = None
        try:
            logger.debug(f"{url} | params = {str(params)}")
            response = requests.get(url=url, params=params, headers=headers, timeout=30)
        except ConnectTimeout as error:
            logger.error(error)
        except Exception as error:
            logger.error(error)

        if response is not None and response.status_code == requests.codes.ok:
            logger.debug(f"{url} | response = {response.text}")
            return response.text

        return None

    def search_city(self, city: str, userdata: UserParameters) -> dict | None:
        """
        Поиск населенного пукнта по его названию

        :param city:     название населенного пункта
        :param userdata: параметры текущего пользователя
        :return:         возвращаемый словарь с населенными пунктами
        """
        params = {"query": city, "locale": userdata.language_api, "currency": userdata.currency}
        data = self.request(self.base_url + self.entry_search, params)

        if data is not None:
            pattern = r'(?<="CITY_GROUP",).+?[\]]'
            find = re.search(pattern, data)
            if find:
                return json.loads(f"{{{find[0]}}}")

        return None

    def lowprice(self, data: UserParameters) -> list | None:
        """
        Поиск отелей с сортировкой по цене, самый дешевый стоит первым
        """
        params = {
            "destinationId": data.city_id,
            "pageNumber": '1',
            "pageSize": data.hotelscount,
            "checkIn": str(data.begin_date),
            "checkOut": str(data.end_date),
            "adults1": '1',
            "sortOrder": 'PRICE',
            "locale": data.language_api,
            "currency": data.currency,
            "priceMin": data.min_price,
            "priceMax": data.max_price
        }
        data = self.request(self.base_url + self.entry_detail, params)

        if data is not None:
            results = json.loads(data)
            return self.get_node(results, ["data", "body", "searchResults", "results"])

        return None

    def highprice(self, data: UserParameters) -> list | None:
        """
        Поиск отелей по цене, самый дорогой стоит первым
        """

        params = {
            "destinationId": data.city_id,
            "pageNumber": '1',
            "pageSize": data.hotelscount,
            "checkIn": str(data.begin_date),
            "checkOut": str(data.end_date),
            "adults1": '1',
            "sortOrder": 'PRICE_HIGHEST_FIRST',
            "locale": data.language_api,
            "currency": data.currency,
            "priceMin": data.min_price,
            "priceMax": data.max_price
        }
        data = self.request(self.base_url + self.entry_detail, params)

        if data is not None:
            results = json.loads(data)
            return self.get_node(results, ["data", "body", "searchResults", "results"])

        return None

    def bestdeal(self, userdata: UserParameters) -> list | None:
        """
        Поиск отелей по цене, с сортировкой по удаленности от центра города
        """

        # Список отелей, удовлетворяющих условию
        hotels_agree = []
        quit_from = False
        pagenumber = 0

        while len(hotels_agree) < userdata.hotelscount and quit_from is False:
            if pagenumber > 20:
                # Больше 20 страниц не смотрим, бесполезно
                break

            pagenumber += 1
            params = {
                "destinationId": userdata.city_id,
                "pageNumber": pagenumber,
                "pageSize": 25,                         # Запрашиваем всегда максимальное количество, т. к. придется фильтровать руками
                "checkIn": str(userdata.begin_date),
                "checkOut": str(userdata.end_date),
                "adults1": '1',
                "sortOrder": 'DISTANCE_FROM_LANDMARK',
                "locale": userdata.language_api,
                "currency": userdata.currency,
                "priceMin": userdata.min_price,
                "priceMax": userdata.max_price
            }
            data = self.request(self.base_url + self.entry_detail, params)

            if data is not None:
                results = json.loads(data)
                hotels = self.get_node(results, ["data", "body", "searchResults", "results"])

                # Нужно составить список отелей удовлетворяющих критериям по расстоянию
                for hotel in hotels:
                    distance = None
                    if len(hotel.get('landmarks', list())) > 0:
                        distance = hotel['landmarks'][0].get('distance', None)

                    # Тег по расстоянию не найден, пропускаем отель
                    if distance is None:
                        continue

                    # Вырезаем лишнее из тега расстояния
                    distance = distance.replace(",", ".")
                    try:
                        distance = float(re.sub(r'[^0123456789\.]', '', distance))
                    except Exception as error:
                        logger.error(error)
                        continue

                    # Проверяем на соответствие критериям
                    if distance >= userdata.min_distance and distance <= userdata.max_distance:
                        hotels_agree.append(hotel)

                        logger.debug(
                            f"Отель {hotel.get('name', '')} подошел по удаленности от центра. | distance = {distance}")
                    else:
                        logger.debug(
                            f"Отель {hotel.get('name', '')} не подходит по удаленности от центра. | distance = {distance}")

                    # Если расстояние больше максимального, то нет смысла проверять дальше
                    if distance > userdata.max_distance or len(hotels_agree) >= userdata.hotelscount:
                        quit_from = True
                        break

        return hotels_agree

    def get_photos(self, hotel_id: int | str, userdata: UserParameters) -> list[str] | None:
        """
        Получить информацию о фотографиях об отеле
        """
        params = {
            "id": hotel_id
        }
        data = self.request(self.base_url + self.entry_photos, params)

        if data is not None:
            results = json.loads(data)
            hotelImages = self.get_node(results, ["hotelImages"])
            if type(hotelImages) == list:
                result = list()

                number = 1
                for image in hotelImages:
                    number += 1

                    try:
                        result.append(image['baseUrl'].format(size=image['sizes'][0]['suffix']))
                    except Exception as error:
                        logger.error(error)

                    if number > userdata.photoscount:
                        break

                return result

        return None
