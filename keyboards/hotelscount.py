from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton

def hotelscount_markup() -> InlineKeyboardMarkup:
    """
    Выбор количества отелей для загрузки
    """
    count_markup = InlineKeyboardMarkup()
    count_markup.row_width = 4
    buttons = [InlineKeyboardButton(text=i, callback_data=f"hotel_{i}") for i in range(1, 24, 2)]
    count_markup.add(*buttons)

    return count_markup