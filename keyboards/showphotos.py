from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from lang.labels import get_message


def showphotos_markup(lang: str = "ru") -> InlineKeyboardMarkup:
    """
    Да / Нет
    """
    markup = InlineKeyboardMarkup()
    markup.row_width = 2
    markup.add(InlineKeyboardButton(text=get_message("YES", lang=lang), callback_data="sp_yes"),
               InlineKeyboardButton(text=get_message("NO", lang=lang), callback_data="sp_no"))

    return markup
