from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from lang.labels import get_message


def confirmation_markup(lang: str | None = None) -> InlineKeyboardMarkup:
    """
    Подтверждение правильности введенных параметров
    """
    markup = InlineKeyboardMarkup()
    markup.row_width = 2
    markup.add(InlineKeyboardButton(text=get_message("CONFIRM_AGREE", lang=lang), callback_data="confirm_yes"),
               InlineKeyboardButton(text=get_message("CONFIRM_NOTAGREE", lang=lang), callback_data="confirm_no"))

    return markup
