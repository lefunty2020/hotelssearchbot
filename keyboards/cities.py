from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from lang.labels import get_message


def cities_markup(cities: dict, lang: str = "ru") -> InlineKeyboardMarkup:
    """
    Выбор варианта населенного пункта для уточнения запроса

    :param cities: словарь населенных пунктов
    :param lang: язык отображения сообщений
    """
    count_markup = InlineKeyboardMarkup()
    count_markup.row_width = 1
    buttons = [InlineKeyboardButton(text=item[1], callback_data=f"city_{item[0]}") for item in cities.items()]
    buttons.insert(0, InlineKeyboardButton(text=get_message('SEARCH_AGAIN', lang=lang), callback_data="city_0"))
    count_markup.add(*buttons)

    return count_markup
