from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from lang.labels import get_message


def history_detail(history_id: int, lang: str | None = None, more: bool = False,
                   first: int = 0) -> InlineKeyboardMarkup:
    """
    Клавиатура для показа кнопки в списке истории, для показа детальной информации по запросу

    :param history_id: идентификатор элемента в истории
    :param lang:       язык для вывода
    :param more:       показать кнопку "еще"
    :param first:      начальный элемент для показа еще
    """
    markup = InlineKeyboardMarkup()
    markup.row_width = 1
    markup.add(InlineKeyboardButton(text=get_message('KEYB_HISTORY_DETAIL', lang=lang),
                                    callback_data=f"historydetail_{history_id}"))

    if more:
        markup.add(InlineKeyboardButton(text=get_message('KEYB_HISTORY_LIST', lang=lang),
                                        callback_data=f"historylist_{first}"))

    return markup