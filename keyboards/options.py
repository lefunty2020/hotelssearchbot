from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from lang.labels import get_message


def lang_markup() -> InlineKeyboardMarkup:
    """
    Выбор языка работы бота, а также запросов к веб-сервису
    """
    markup = InlineKeyboardMarkup()
    markup.row_width = 2
    markup.add(InlineKeyboardButton(text="English", callback_data="lang_english"),
               InlineKeyboardButton(text="Русский", callback_data="lang_russian"))

    return markup


def currency_markup() -> InlineKeyboardMarkup:
    """
    Выбор валюты
    """
    markup = InlineKeyboardMarkup()
    markup.row_width = 3
    markup.add(InlineKeyboardButton(text="USD", callback_data="currency_USD"),
               InlineKeyboardButton(text="EUR", callback_data="currency_EUR"),
               InlineKeyboardButton(text="RUB", callback_data="currency_RUB"))

    return markup


def options_markup(lang: str | None = None) -> InlineKeyboardMarkup:
    """
    Главное меню в разделе опции
    """
    markup = InlineKeyboardMarkup()
    markup.row_width = 2
    markup.add(
        InlineKeyboardButton(text=get_message('BACK', lang=lang), callback_data="options_back"))
    markup.add(
        InlineKeyboardButton(text=get_message('OPTIONS_SHOW_CURRENT', lang=lang), callback_data="options_show_current"))
    markup.add(
        InlineKeyboardButton(text=get_message('OPTIONS_SELECT_LANGUAGE', lang=lang), callback_data="options_language"),
        InlineKeyboardButton(text=get_message('OPTIONS_SELECT_CURRENCY', lang=lang), callback_data="options_currency"))

    return markup
