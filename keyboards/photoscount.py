from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton


def photoscount_markup() -> InlineKeyboardMarkup:
    """
    Выбор количества загружаемых фотографий
    """
    count_markup = InlineKeyboardMarkup()
    count_markup.row_width = 5
    buttons = [InlineKeyboardButton(text=i, callback_data=f"photos_{i}") for i in range(1, 11)]
    count_markup.add(*buttons)

    return count_markup
