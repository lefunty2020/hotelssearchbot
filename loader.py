import logging

import telebot
from loguru import logger
from telebot import TeleBot
from telebot.storage import StateMemoryStorage
from config_data import config
from api.rapidapi import RapidApi
from database.sqlite import *


def get_log_message(text: str, **kwargs) -> str:
    """
    Получить отформатированное сообщение с дополнительными переданными параметрами

    :param text:
    :param kwargs:
    :return: str
    """
    result = text
    for key, value in kwargs.items():
        result += f"\t| {key} = {value}"

    return result


class ExceptionHandler(telebot.ExceptionHandler):
    """
    Обработчик неперехваченных ошибок
    """
    def handle(self, exception):
        logger.error(exception)


logger.add('logs/hotelsearchbot.log', level='DEBUG', rotation="00:00")
logger.info("Запуск бота.")

db.create_tables([History, Messages, Photos, Options])

storage = StateMemoryStorage()
bot = TeleBot(token=config.BOT_TOKEN, state_storage=storage, exception_handler=ExceptionHandler())

rapid = RapidApi()
rapid.api_key = config.RAPID_API_KEY

