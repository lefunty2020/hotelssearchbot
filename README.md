#  Бот поиска отелей Hotels.com
Бот реализует функционал поиска отелей посредством сервиса Hotels.com

Доступные команды
/start      - Начало работы
/options    - Установка параметров работы бота
/history    - Просмотр истории запросов с возможностью вывода результата запросов
/help       - Справка
/lowprice   - Запрос с сортировкой по минимальной цене
/highprice  - Запрос с сортировкой по максимальной цене
/bestdeal   - Запрос с фильтрацией по расстоянию от центра

