from . import en
from . import ru


class Labels:
    """
    Класс для локализации
    """

    @staticmethod
    def get_message(message_id: str | int, lang: str | None = None, replace: dict = None) -> str:
        result = False
        if lang == "en":
            result = en.messages.messages.get(message_id, False)

        if result is False:
            result = ru.messages.messages.get(message_id, False)

        # Если есть подстановки, то заменяем
        if replace is not None:
            for key, value in replace.items():
                result = result.replace(f"#{key}#", str(value))

        return result


def get_message(message_id: str | int, replace: dict = None, lang: str | None = None) -> str:
    """
    Получить локализованное сообщение по идентификатору
    """

    return Labels.get_message(message_id, lang=lang, replace=replace)
