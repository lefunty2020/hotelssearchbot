
def singleton(cls):
    def wrapper(*args, **kwargs):
        if wrapper.instance is None:
            wrapper.instance = cls(*args, **kwargs)

        return wrapper.instance

    wrapper.instance = None
    return wrapper
