import io
from loguru import logger
from telebot.types import Message, CallbackQuery, InputMediaPhoto
from loader import bot, get_log_message
from database.sqlite import db, History, Messages
from states.request_parameters import ParametersManager
from keyboards.history import *


def show_history(user_id, chat_id, first, replay_message: int | None = None):
    with db:
        commands = (History.select()
                    .where(History.user_id == user_id)
                    .order_by(History.record_date.desc())
                    .offset(first)
                    .limit(11))

        userdata = ParametersManager().get_parameters(user_id)
        if replay_message is None:
            bot.send_message(chat_id=chat_id, text=f"{get_message('HISTORY', lang=userdata.language)}")
        else:
            bot.edit_message_text(chat_id=chat_id, text=f"{get_message('HISTORY', lang=userdata.language)}",
                                  message_id=replay_message)

        index = 0
        count = len(commands)
        for command in commands:
            index += 1
            command_history = f"{get_message('KEYB_HISTORY_DATE', lang=userdata.language)} " \
                              f"{command.record_date.strftime(userdata.datetime_format)}\n" \
                              f"{command.parameters}"

            more = True if index == 10 and count > 10 else False
            try:
                bot.send_message(chat_id=chat_id, text=command_history, parse_mode='HTML',
                                 reply_markup=history_detail(command.id, lang=userdata.language, more=more,
                                                             first=(first + 10)))
            except Exception as error:
                logger.error(error)

            if index == 10:
                break


@bot.message_handler(commands=['history'])
def bot_history(message: Message):
    """
    Вывод последних 10 команд
    """

    logger.info(get_log_message(message.text, user_id=message.from_user.id))
    show_history(message.from_user.id, message.chat.id, 0)


@bot.callback_query_handler(func=lambda call: call.data.startswith('historylist_') is True)
def historylist_query(call: CallbackQuery):
    """
    Обработчик нажатия кнопки, показать еще 10 команд в истории
    """
    parts = call.data.split(sep="_")
    first = int(parts[1])

    show_history(call.from_user.id, call.message.chat.id, first, replay_message=call.message.id)


@bot.callback_query_handler(func=lambda call: call.data.startswith('historydetail_') is True)
def historydetail_query(call: CallbackQuery):
    """
    Обработчик кнопки показать детальную информацию по запросу
    """

    parts = call.data.split(sep="_")
    history_id = int(parts[1])

    with db:
        messages = (Messages.select()
                    .where(Messages.history_id == history_id))

        for message in messages:
            media = []
            caption = message.caption

            for photo in message.photos:
                stream = io.BytesIO(photo.data)
                media.append(InputMediaPhoto(media=stream, parse_mode='HTML', caption=caption))
                caption = None

            try:
                if len(media) > 0:
                    bot.send_media_group(chat_id=call.message.chat.id, media=media)
                else:
                    bot.send_message(chat_id=call.message.chat.id, text=caption, parse_mode='HTML', disable_web_page_preview=True)
            except Exception as error:
                logger.error(error)

