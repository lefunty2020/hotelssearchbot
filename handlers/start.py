from loguru import logger
from telebot.types import Message, CallbackQuery
from config_data.config import DEFAULT_COMMANDS
from loader import bot, get_log_message
from keyboards.options import *
from states.request_parameters import ParametersManager
from database.sqlite import History


def show_option_menu(chat_id: int | str, message_id: int | None = None, lang: str | None = None):
    """
    Отобразить главное меню настроек
    """
    if message_id != None:
        bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                              text=get_message('OPTIONS_MENU', lang=lang),
                              reply_markup=options_markup(lang=lang))
    else:
        bot.send_message(chat_id=chat_id, text=get_message('OPTIONS_MENU', lang=lang),
                         reply_markup=options_markup(lang=lang))


def show_current_settings(user_id: int, chat_id: int | str, message_id: int | None = None):
    """
    Вывести сообщение с текущими настройками
    """
    if message_id is None:
        bot.send_message(chat_id=chat_id, text=ParametersManager().get_parameters_str(user_id),
                         parse_mode='HTML')
    else:
        bot.edit_message_text(chat_id=chat_id, message_id=message_id,
                              text=ParametersManager().get_parameters_str(user_id),
                              parse_mode='HTML')


def show_help(user_id: int, chat_id: int | str, message: Message | None = None):
    """
    Вывести справку
    """
    userdata = ParametersManager().get_parameters(user_id)
    text = ['/{command} - {desk}'.format(command=command,
                                         desk=get_message('COMMAND_' + command.upper(), lang=userdata.language)) for
            command, desk in DEFAULT_COMMANDS]

    if message is None:
        bot.send_message(chat_id=chat_id, text='\n'.join(text))
    else:
        bot.reply_to(message, '\n'.join(text))


@bot.message_handler(commands=['start'])
def bot_start(message: Message):
    """
    Поприветствовать пользователя и выдать первоначальную информацию
    """
    logger.info(get_log_message(message.text, user_id=message.from_user.id))

    userdata = ParametersManager().get_parameters(message.from_user.id)

    bot.reply_to(message, get_message("HELLO", {"USERNAME": message.from_user.full_name}, lang=userdata.language))
    show_help(user_id=message.from_user.id, chat_id=message.chat.id)
    show_current_settings(user_id=message.from_user.id, chat_id=message.chat.id)


@bot.message_handler(commands=['help'])
def bot_help(message: Message):
    """
    Отобразить справочную информацию
    """
    logger.info(get_log_message(message.text, user_id=message.from_user.id))

    show_help(user_id=message.from_user.id, chat_id=message.chat.id, message=message)


@bot.message_handler(commands=['options'])
def bot_options(message: Message):
    """
    Вывод главного меню в разделе опции
    """
    logger.info(get_log_message(message.text, user_id=message.from_user.id))

    userdata = ParametersManager().get_parameters(message.from_user.id)
    show_option_menu(chat_id=message.chat.id, lang=userdata.language)


@bot.callback_query_handler(func=lambda call: call.data.startswith('options_') is True)
def bot_options(call: CallbackQuery):
    """
    Вывод подразделов из меню опции
    """
    userdata = ParametersManager().get_parameters(call.from_user.id)
    if call.data == "options_language":
        logger.info(get_log_message(call.data, user_id=call.from_user.id))

        userdata = ParametersManager().get_parameters(call.from_user.id)
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.id,
                              text=get_message('SELECT_LANGUAGE', lang=userdata.language),
                              reply_markup=lang_markup())
    elif call.data == "options_currency":
        logger.info(get_log_message(call.data, user_id=call.from_user.id))

        userdata = ParametersManager().get_parameters(call.from_user.id)
        bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.id,
                              text=get_message('SELECT_CURRENCY', lang=userdata.language),
                              reply_markup=currency_markup())
    elif call.data == "options_show_current":
        show_current_settings(user_id=call.from_user.id, chat_id=call.message.chat.id, message_id=call.message.id)
        show_option_menu(chat_id=call.message.chat.id, lang=userdata.language)
    elif call.data == "options_back":
        show_current_settings(user_id=call.from_user.id, chat_id=call.message.chat.id, message_id=call.message.id)


@bot.callback_query_handler(func=lambda call: call.data.startswith('lang_') is True)
def options_query(call: CallbackQuery):
    """
    Обработчик выбора языка работы бота
    """
    userdata = ParametersManager().get_parameters(call.from_user.id)
    if call.data == "lang_english":
        userdata.language = "en"
        userdata.language_api = "en_US"
        userdata.datetime_format = "%m/%d/%Y %H:%M:%S"
        userdata.date_format = "%m/%d/%Y"
        userdata.time_format = "%H:%M:%S"

        bot.edit_message_text(text="Bot language: <b>English</b>", chat_id=call.message.chat.id, message_id=call.message.id,
                              parse_mode='HTML')
        logger.info(get_log_message("Выбран язык бота: <b>English</b>", user_id=call.from_user.id))
    else:
        userdata.language = "ru"
        userdata.language_api = "ru_RU"
        userdata.datetime_format = "%d.%m.%Y %H:%M:%S"
        userdata.date_format = "%d.%m.%Y"
        userdata.time_format = "%H:%M:%S"

        bot.edit_message_text(text=f"Язык бота: <b>Русский</b>", chat_id=call.message.chat.id, message_id=call.message.id,
                              parse_mode='HTML')
        logger.info(get_log_message("Выбран язык бота: Русский", user_id=call.from_user.id))

    userdata.config_loaded = True
    ParametersManager().set_parameters(call.from_user.id, userdata)

    show_option_menu(chat_id=call.message.chat.id, lang=userdata.language)


@bot.callback_query_handler(func=lambda call: call.data.startswith('currency_') is True)
def options_query(call: CallbackQuery):
    """
    Обработчик выбора валюты для выполнения запросов к веб-сервису
    """
    userdata = ParametersManager().get_parameters(call.from_user.id)
    if call.data == "currency_USD":
        userdata.currency = "USD"
    elif call.data == "currency_EUR":
        userdata.currency = "EUR"
    else:
        userdata.currency = "RUB"

    bot.edit_message_text(text=f"{get_message('LABEL_CURRENCY')}: <b>{userdata.currency}</b>",
                          chat_id=call.message.chat.id, message_id=call.message.id, parse_mode='HTML')
    logger.info(get_log_message(f"Выбрана валюта работы: {userdata.currency}", user_id=call.from_user.id))

    userdata.config_loaded = True
    ParametersManager().set_parameters(call.from_user.id, userdata)

    show_option_menu(chat_id=call.message.chat.id, lang=userdata.language)
