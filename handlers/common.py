import datetime
import io
import json

import requests
from requests.exceptions import ConnectTimeout
from loguru import logger
from telebot import types
from telebot.types import Message, CallbackQuery
from loader import bot, rapid, get_log_message
from lang.labels import get_message
from datetime import date, timedelta
import re
from states.request_parameters import RequestParametersState
from telegram_bot_calendar import DetailedTelegramCalendar, LSTEP
from keyboards.photoscount import photoscount_markup
from keyboards.hotelscount import hotelscount_markup
from keyboards.showphotos import showphotos_markup
from keyboards.cities import cities_markup
from keyboards.confirmation import confirmation_markup
from states.request_parameters import UserParameters, ParametersManager
from database.sqlite import History, Messages, Photos
from peewee import Model


class MyCustomCalendar(DetailedTelegramCalendar):
    empty_month_button = ""
    empty_year_button = ""


@bot.message_handler(commands=['bestdeal', 'lowprice', 'highprice'])
def bot_common(message: Message):
    """
    Обработчик вызова команд  /bestdeal, /lowprice, /highprice
    """

    logger.info(get_log_message(message.text, user_id=message.from_user.id))

    userdata = ParametersManager().get_parameters(message.from_user.id)
    calendar, step = MyCustomCalendar(min_date=date.today(), locale=userdata.language, calendar_id=1).build()
    bot.send_message(chat_id=message.chat.id,
                     text=get_message("SELECT_CHECKIN_DATE",
                                      {"STEP": get_message(LSTEP[step].upper(), lang=userdata.language)},
                                      lang=userdata.language),
                     reply_markup=calendar)

    ParametersManager().set_parameter(message.from_user.id, UserParameters.COMMAND, message.text)
    bot.set_state(user_id=message.from_user.id, state=RequestParametersState.date_enter, chat_id=message.chat.id)


@bot.callback_query_handler(func=MyCustomCalendar.func(calendar_id=1))
def begindate_call(c: CallbackQuery):
    """
    Обработчик выбора даты начала периода
    """
    userdata = ParametersManager().get_parameters(c.from_user.id)
    result, key, step = MyCustomCalendar(min_date=date.today(), locale=userdata.language,
                                                 calendar_id=1).process(c.data)
    if not result and key:
        bot.edit_message_text(
            text=get_message("SELECT_CHECKIN_DATE", {"STEP": get_message(LSTEP[step].upper(), lang=userdata.language)},
                             lang=userdata.language),
            chat_id=c.message.chat.id,
            message_id=c.message.message_id,
            reply_markup=key)
    elif result:
        bot.edit_message_text(
            text=get_message("CHECKIN_DATE", {"DATE": result.strftime(userdata.date_format)}, lang=userdata.language),
            chat_id=c.message.chat.id,
            message_id=c.message.message_id)

        ParametersManager().set_parameter(c.from_user.id, UserParameters.BEGIN_DATE, result)
        bot.set_state(user_id=c.from_user.id, state=RequestParametersState.date_enter, chat_id=c.message.chat.id)

        # Создаем второй календарь, минимальную дату указываем не раньше даты начала периода
        calendar, step = MyCustomCalendar(min_date=result + timedelta(days=1), locale=userdata.language, calendar_id=2).build()
        bot.send_message(chat_id=c.message.chat.id,
                         text=get_message("SELECT_CHECKOUT_DATE",
                                          {"STEP": get_message(LSTEP[step].upper(), lang=userdata.language)},
                                          lang=userdata.language),
                         reply_markup=calendar)


@bot.callback_query_handler(func=MyCustomCalendar.func(calendar_id=2))
def enddate_call(c: CallbackQuery):
    """
    Обработчик выбора даты окончания периода
    """

    userdata = ParametersManager().get_parameters(c.from_user.id)
    result, key, step = MyCustomCalendar(min_date=userdata.begin_date + timedelta(days=1), locale=userdata.language,
                                                 calendar_id=2).process(c.data)
    if not result and key:
        bot.edit_message_text(
            text=get_message("SELECT_CHECKOUT_DATE", {"STEP": get_message(LSTEP[step].upper(), lang=userdata.language)},
                             lang=userdata.language),
            chat_id=c.message.chat.id,
            message_id=c.message.message_id,
            reply_markup=key)
    elif result:
        bot.edit_message_text(
            text=get_message("CHECKOUT_DATE", {"DATE": result.strftime(userdata.date_format)}, lang=userdata.language),
            chat_id=c.message.chat.id,
            message_id=c.message.message_id)

        bot.send_message(chat_id=c.message.chat.id, text=get_message("ENTER_CITY", lang=userdata.language))
        ParametersManager().set_parameter(c.from_user.id, UserParameters.END_DATE, result)
        bot.set_state(user_id=c.from_user.id, state=RequestParametersState.city, chat_id=c.message.chat.id)


@bot.message_handler(state=RequestParametersState.city)
def city_call(message: Message):
    """
    Обработчик ввода населенного пункта
    """
    userdata = ParametersManager().get_parameters(message.from_user.id)
    cities = rapid.search_city(message.text, userdata)

    if cities is not None and len(cities["entities"]) > 0:
        bot.set_state(user_id=message.from_user.id, state=RequestParametersState.citydetail, chat_id=message.chat.id)
        cities_keyb = dict()
        userdata.temp_data['cities'] = dict()
        for entity in cities.get("entities", False):
            caption = re.sub(r'<[^<>]*>', '', entity["caption"])
            cities_keyb[entity["destinationId"]] = caption
            userdata.temp_data['cities'][entity["destinationId"]] = caption

        bot.send_message(chat_id=message.chat.id, text=get_message("CLARIFY_CITY", lang=userdata.language),
                         reply_markup=cities_markup(cities_keyb, lang=userdata.language))
    else:
        bot.send_message(chat_id=message.chat.id, text=get_message("CITY_NOTFOUND", lang=userdata.language))
        logger.warning(get_log_message(get_message("CITY_ENTER_AGAIN", lang=userdata.language),
                                       user_id=message.from_user.id, city_name=message.text))


@bot.callback_query_handler(func=lambda call: call.data.startswith('city_') is True)
def citydetail_query(call: CallbackQuery):
    """
    Обработчик выбора уточненного населенного пункта
    """

    parameters = ParametersManager()
    userdata = parameters.get_parameters(call.from_user.id)

    parts = call.data.split(sep="_")
    city_id = parts[1]
    city_name = userdata.temp_data['cities'].get(city_id, '')
    if int(city_id) > 0:
        bot.edit_message_text(text=get_message("CITY_ENTERED", {"CITY": city_name}, lang=userdata.language),
                              chat_id=call.message.chat.id, message_id=call.message.id)

        parameters.set_parameter(call.from_user.id, UserParameters.CITY_ID, city_id)
        parameters.set_parameter(call.from_user.id, UserParameters.CITY_NAME, city_name)

        bot.set_state(user_id=call.from_user.id, state=RequestParametersState.prices, chat_id=call.message.chat.id)
        bot.send_message(chat_id=call.message.chat.id,
                         text=get_message("PRICE_ENTER", replace={'CURRENCY': userdata.currency},
                                          lang=userdata.language))
    else:
        bot.set_state(user_id=call.from_user.id, state=RequestParametersState.city, chat_id=call.message.chat.id)
        bot.edit_message_text(text=get_message("CITY_ENTER_AGAIN", lang=userdata.language), chat_id=call.message.chat.id,
                              message_id=call.message.id)


@bot.message_handler(state=RequestParametersState.prices)
def prices_call(message: Message):
    """
    Обработчик ввода ограничений по ценам
    """
    userdata = ParametersManager().get_parameters(message.from_user.id)
    parts = [part.strip() for part in message.text.split("-")]

    # Проверка входных данных на корректность
    if len(parts) < 2:
        bot.send_message(chat_id=message.chat.id, text=get_message("PRICE_ERROR", lang=userdata.language))
        logger.warning(get_log_message(get_message("PRICE_ERROR", lang=userdata.language), user_id=message.from_user.id),
                       message_text=message.text)
        return

    for index in range(2):
        if parts[index] != '' and parts[index].isdigit() is not True:
            bot.send_message(chat_id=message.chat.id, text=get_message("PRICE_ERROR2", lang=userdata.language))
            logger.warning(
                get_log_message(get_message("PRICE_ERROR2", lang=userdata.language), user_id=message.from_user.id),
                message_text=message.text)
            return

    min_price = int(parts[0]) if parts[0] != '' else 0
    max_price = int(parts[1]) if parts[1] != '' else UserParameters.max_price_limit;
    if max_price is not None and min_price >= max_price:
        bot.send_message(chat_id=message.chat.id, text=get_message("PRICE_ERROR3", lang=userdata.language))
        logger.warning(get_log_message(get_message("PRICE_ERROR3", lang=userdata.language), user_id=message.from_user.id),
                       message_text=message.text)
        return

    # Все проверки прошли, записываем данные и идем дальше
    parameters = ParametersManager()
    parameters.set_parameter(message.from_user.id, UserParameters.MIN_PRICE, min_price)
    parameters.set_parameter(message.from_user.id, UserParameters.MAX_PRICE, max_price)

    # Проверяем какая команда была введена, если была введена команда /bestdeal
    # то дополнительно запрашиваем диапазон расстояний
    if userdata.command == '/bestdeal':
        bot.set_state(user_id=message.from_user.id, state=RequestParametersState.distance, chat_id=message.chat.id)
        bot.send_message(chat_id=message.chat.id, text=get_message("DISTANCE_ENTER", lang=userdata.language))
    else:
        bot.set_state(user_id=message.from_user.id, state=RequestParametersState.hotelscount, chat_id=message.chat.id)
        bot.send_message(chat_id=message.chat.id, text=get_message("HOTELS_COUNT_REQUEST", lang=userdata.language),
                         reply_markup=hotelscount_markup())


@bot.message_handler(state=RequestParametersState.distance)
def distance_call(message: Message):
    """
    Обработчик ввода ограничений по расстоянию от центра
    """
    userdata = ParametersManager().get_parameters(message.from_user.id)
    parts = [part.strip() for part in message.text.split("-")]

    # Проверка на правильность ввода
    if len(parts) < 2:
        bot.send_message(chat_id=message.chat.id, text=get_message("DISTANCE_ERROR", lang=userdata.language))
        logger.warning(get_log_message(get_message("DISTANCE_ERROR", lang=userdata.language), user_id=message.from_user.id),
                       message_text=message.text)
        return None

    # Проверяем части на правильность ввода
    for index in range(2):
        if parts[index] != '':
            try:
                float(parts[index])
            except Exception as error:
                bot.send_message(chat_id=message.chat.id, text=get_message("DISTANCE_ERROR2", lang=userdata.language))
                logger.warning(
                    get_log_message(get_message("DISTANCE_ERROR2", lang=userdata.language), user_id=message.from_user.id),
                    message_text=message.text)

                return None

    min_distance = float(parts[0]) if parts[0] != '' else 0
    max_distance = float(parts[1]) if parts[1] != '' else UserParameters.max_distance_limit;
    if max_distance is not None and min_distance >= max_distance:
        bot.send_message(chat_id=message.chat.id, text=get_message("DISTANCE_ERROR3", lang=userdata.language))
        logger.warning(get_log_message(get_message("DISTANCE_ERROR3", lang=userdata.language), user_id=message.from_user.id),
                       message_text=message.text)
        return None

    # Все проверки прошли, записываем данные и идем дальше
    parameters = ParametersManager()
    parameters.set_parameter(message.from_user.id, UserParameters.MIN_DISTANCE, min_distance)
    parameters.set_parameter(message.from_user.id, UserParameters.MAX_DISTANCE, max_distance)

    # Запрашиваем количество отображаемых отелей
    bot.set_state(user_id=message.from_user.id, state=RequestParametersState.hotelscount, chat_id=message.chat.id)
    bot.send_message(chat_id=message.chat.id, text=get_message("HOTELS_COUNT_REQUEST", lang=userdata.language),
                     reply_markup=hotelscount_markup())


@bot.callback_query_handler(func=lambda call: call.data.startswith('hotel_') is True)
def hotels_query(call: CallbackQuery):
    """
    Обработчик выбора количества показываемых отелей
    """

    userdata = ParametersManager().get_parameters(call.from_user.id)
    hotelcount = call.data[6:]
    bot.edit_message_text(text=get_message('HOTELS_COUNT', {'COUNT': hotelcount}, lang=userdata.language),
                          chat_id=call.message.chat.id, message_id=call.message.id)

    bot.set_state(user_id=call.from_user.id, state=RequestParametersState.showphotos, chat_id=call.message.chat.id)
    ParametersManager().set_parameter(call.from_user.id, UserParameters.HOTELSCOUNT, int(hotelcount))

    bot.send_message(chat_id=call.message.chat.id, text=get_message("SHOW_PHOTOS_REQUEST", lang=userdata.language),
                     reply_markup=showphotos_markup(lang=userdata.language))


def get_request_parameters_str(userdata: UserParameters) -> str:
    """
    Получить отформатированную строку с параметрами для запроса
    """
    begin_date = userdata.begin_date.strftime(userdata.date_format)
    end_date = userdata.end_date.strftime(userdata.date_format)
    show_photos = userdata.photoscount if userdata.showphotos else get_message('NO', lang=userdata.language)
    max_price = '{:,d}'.format(userdata.max_price) if userdata.max_price < UserParameters.max_price_limit else \
        get_message('PRICE_NO_LIMIT', lang=userdata.language)
    message = f"{get_message('COMMAND_NAME', lang=userdata.language)} <b>{userdata.command[1:]}</b>\n" \
              f"{get_message('CONFIRM_BEGIN_DATE', lang=userdata.language)} <b>{begin_date}</b>\n" \
              f"{get_message('CONFIRM_END_DATE', lang=userdata.language)} <b>{end_date}</b>\n" \
              f"{get_message('CONFIRM_CITY', lang=userdata.language)} <b>{userdata.city_name}</b>\n" \
              f"{get_message('LABEL_CURRENCY', lang=userdata.language)} <b>{userdata.currency}</b>\n" \
              f"{get_message('CONFIRM_MIN_PRICE', lang=userdata.language)} <b>{'{:,d}'.format(userdata.min_price)}</b>\n" \
              f"{get_message('CONFIRM_MAX_PRICE', lang=userdata.language)} <b>{max_price}</b>\n"

    if userdata.command == "/bestdeal":
        message += f"{get_message('CONFIRM_MIN_DISTANCE', lang=userdata.language)} <b>{round(userdata.min_distance, 2)}</b>\n" \
                   f"{get_message('CONFIRM_MAX_DISTANCE', lang=userdata.language)} <b>{round(userdata.max_distance, 2)}</b>\n"

    message += f"{get_message('CONFIRM_HOTELS_COUNT', lang=userdata.language)} <b>{userdata.hotelscount}</b>\n" \
               f"{get_message('CONFIRM_SHOW_PHOTO', lang=userdata.language)} <b>{show_photos}</b>"

    return message


@bot.callback_query_handler(func=lambda call: call.data.startswith('sp_') is True)
def showphotos_query(call: CallbackQuery):
    """
    Обработчик выбора признака показывать фото или нет
    """

    userdata = ParametersManager().get_parameters(call.from_user.id)
    showphotos = True if call.data == "sp_yes" else False
    ParametersManager().set_parameter(call.from_user.id, UserParameters.SHOWPHOTOS, showphotos)

    if showphotos:
        bot.edit_message_text(
            text=get_message("SHOW_PHOTOS", {"ANSWER": get_message("YES", lang=userdata.language)},
                             lang=userdata.language),
            chat_id=call.message.chat.id, message_id=call.message.id)
        bot.set_state(user_id=call.from_user.id, state=RequestParametersState.photoscount, chat_id=call.message.chat.id)

        # Выводим запрос, сколько фотографий показывать
        bot.send_message(chat_id=call.message.chat.id, text=get_message("PHOTOS_COUNT_REQUEST", lang=userdata.language),
                         reply_markup=photoscount_markup())
    else:
        bot.set_state(user_id=call.from_user.id, state=RequestParametersState.confirmation, chat_id=call.message.chat.id)
        bot.edit_message_text(
            text=get_message("SHOW_PHOTOS", {"ANSWER": get_message("NO", lang=userdata.language)},
                             lang=userdata.language),
            chat_id=call.message.chat.id, message_id=call.message.id)

        msg = f"{get_message('CONFIRM_PARAMETERS', lang=userdata.language)}\n{get_request_parameters_str(userdata)}"
        bot.send_message(chat_id=call.message.chat.id, text=msg,
                         reply_markup=confirmation_markup(lang=userdata.language), parse_mode='HTML')


@bot.callback_query_handler(func=lambda call: call.data.startswith('photos_') is True)
def photoscount_query(call: CallbackQuery):
    """
    Обработчик выбора количества показываемых фотографий
    """

    userdata = ParametersManager().get_parameters(call.from_user.id)
    photoscount = call.data[7:]
    bot.edit_message_text(
        text=get_message("PHOTOS_COUNT", {"ANSWER": photoscount}, lang=userdata.language),
        chat_id=call.message.chat.id,
        message_id=call.message.id)

    bot.set_state(user_id=call.from_user.id, state=RequestParametersState.confirmation, chat_id=call.message.chat.id)
    ParametersManager().set_parameter(call.from_user.id, UserParameters.PHOTOSCOUNT, int(photoscount))

    msg = f"{get_message('CONFIRM_PARAMETERS', lang=userdata.language)}\n{get_request_parameters_str(userdata)}"
    bot.send_message(chat_id=call.message.chat.id, text=msg,
                     reply_markup=confirmation_markup(lang=userdata.language), parse_mode='HTML')


@bot.callback_query_handler(func=lambda call: call.data.startswith('confirm_') is True)
def agreement_query(call: CallbackQuery):
    """
    Если получено подтверждение правильности данных, то делаем запрос
    иначе сбрасываем статус бота и вводим все заново
    """

    userdata = ParametersManager().get_parameters(call.from_user.id)
    if call.data == "confirm_yes":
        bot.set_state(user_id=call.from_user.id, state=RequestParametersState.completed,
                      chat_id=call.message.chat.id)

        msg = f"{get_message('CONFIRM_REQUEST', lang=userdata.language)}\n{get_request_parameters_str(userdata)}"
        bot.edit_message_text(text=msg, chat_id=call.message.chat.id,
                              message_id=call.message.id, parse_mode='HTML')
        new_message = bot.send_message(chat_id=call.message.chat.id,
                                       text=f"{get_message('CONFIRM_REQUEST2', lang=userdata.language)}")
        userdata.temp_data['message_id'] = new_message.id

        route(call.from_user.id, call.message.chat.id)
    elif call.data == "confirm_no":
        bot.set_state(user_id=call.from_user.id, state=RequestParametersState.start,
                      chat_id=call.message.chat.id)

        bot.edit_message_text(text=get_message("CONFIRM_FAIL", lang=userdata.language), chat_id=call.message.chat.id,
                              message_id=call.message.id)


def format_message(user_id: int, item: dict) -> str:
    """
    Отформатировать информацию по отелю для отправки в телеграм
    """
    userdata = ParametersManager().get_parameters(user_id)

    hotelclass = "⭐" * round(item.get('starRating', 0))
    prices = item.get('ratePlan', dict()).get('price', dict())
    price_day_formatted = prices.get('current', '-')

    formats = {'RUB': '{:,d} RUB', 'USD': '${:,d}', 'EUR': '{:,d} €'}

    price_total_str = re.sub(r'&[^&;]*;', ' ', prices.get('fullyBundledPricePerStay', '-'))
    price_total = price_total_str if 'fullyBundledPricePerStay' in prices else formats[userdata.currency].format(
        round(prices.get('exactCurrent', 0) * (userdata.end_date - userdata.begin_date).days))

    # Если общая стоимость не предоставлена сервисом и расчитана скриптом,
    # то проинформировать пользователя об этом
    star = ""
    suphix = ""
    if price_total_str == '-':
        star = " *"
        suphix = f"\n<i>* - {get_message('PRICE_CALC_BOT', lang=userdata.language)}</i>"

    distance = "-"
    if len(item.get('landmarks', list())) > 0:
        distance = item['landmarks'][0].get('distance', '-')

    address = item.get('address', dict()).get('streetAddress', '-');
    url = f'https://www.hotels.com/ho{item["id"]}'

    begin_date = userdata.begin_date.strftime(userdata.date_format)
    end_date = userdata.end_date.strftime(userdata.date_format)
    current_date = datetime.datetime.now().strftime(userdata.datetime_format)

    rating = item.get('guestReviews', dict()).get('unformattedRating', get_message('NO', lang=userdata.language))
    message = f"{get_message('LABEL_HOTEL', replace={'CLASS': hotelclass, 'HOTEL': item['name']}, lang=userdata.language)}\n" \
              f"{get_message('LABEL_RATE', lang=userdata.language)} <b>{rating}</b>\n" \
              f"{get_message('CONFIRM_BEGIN_DATE', lang=userdata.language)} <b>{begin_date}</b>\n" \
              f"{get_message('CONFIRM_END_DATE', lang=userdata.language)} <b>{end_date}</b>\n" \
              f"{get_message('LABEL_PRICE_DAY', lang=userdata.language)} <b>{price_day_formatted}</b>\n" \
              f"{get_message('LABEL_PRICE_TOTAL', lang=userdata.language)} <b>{price_total}</b>{star}\n" \
              f"{get_message('LABEL_DISTANCE', lang=userdata.language)} <b>{distance}</b>\n" \
              f"{get_message('LABEL_ADDRESS', lang=userdata.language)} <b>{address}</b>\n" \
              f"{get_message('LABEL_URL', lang=userdata.language)} <b>{url}</b>" \
              f"{suphix}" \
              f"\n<i>* - {get_message('KEYB_HISTORY_DATE', lang=userdata.language)} {current_date}</i>"


    return message


def print_result_one(user_id: int | str, chat_id: int | str, item: dict, history_id: Model) -> list[Message] | None:
    """
    Отправка в телеграм сообщения по конкретному отелю
    """
    userdata = ParametersManager().get_parameters(user_id)

    # Фотографии запрашиваем по мере вывода информации об отелях, чтобы пользователю они выдавались
    # постепенно, иначе долго придется ждать
    media = []
    photos_to_db = []
    caption_for_history = format_message(user_id, item)
    if userdata.showphotos:
        photos = rapid.get_photos(item['id'], userdata)

        caption = caption_for_history
        if (photos is not None) and (len(photos) > 0):
            for photo in photos:
                try:
                    data = requests.get(url=photo, timeout=10)
                    if data.status_code == requests.codes.ok:
                        logger.info(photo)

                        photos_to_db.append(data.content)
                        stream = io.BytesIO(data.content)
                        media.append(types.InputMediaPhoto(media=stream, parse_mode='HTML', caption=caption))

                        caption = None
                    else:
                        logger.warning(get_log_message("Ошибка получения фото с веб-сервиса", photo=photo))
                except ConnectTimeout as error:
                    logger.error(error)
                except Exception as error:
                    logger.error(error)

    try:
        # Сохраняем текстовое сообщение
        message_id = Messages.create(history=history_id, caption=caption_for_history)

        if len(media) > 0:
            # Сохраняем фото если есть
            for photo in photos_to_db:
                Photos.create(message=message_id, data=photo)

            return bot.send_media_group(chat_id=chat_id, media=media)
        else:
            return [bot.send_message(chat_id=chat_id, text=format_message(user_id, item),
                                     parse_mode='HTML', disable_web_page_preview=True)]

    except Exception as error:
        logger.error(error)

    return None


def print_results(user_id: int | str, chat_id: int | str, results: dict):
    """
    Отображаем результаты поиска отелей
    """
    userdata = ParametersManager().get_parameters(user_id)

    # Сохраняем параметры запроса в истории
    history_id = History.create(chat_id=chat_id, user_id=user_id, command=userdata.command,
                                parameters=get_request_parameters_str(userdata))

    bot.delete_message(chat_id=chat_id, message_id=userdata.temp_data.get("message_id"))
    if results is None:
        bot.send_message(chat_id=chat_id, text=get_message('ERROR_TIMEOUT_EXPIRED', lang=userdata.language))
    elif type(results) == list and len(results) == 0:
        bot.send_message(chat_id=chat_id, text=get_message('ERROR_EMPTY_RESULTS', lang=userdata.language))
    else:
        bot.send_message(chat_id=chat_id,
                         text=get_message("HOTELS_FOUND_HEADER", lang=userdata.language))
        index = 0
        for item in results:
            result = print_result_one(user_id, chat_id, item, history_id)
            if result is not None:
                index += 1

        bot.send_message(chat_id=chat_id,
                         text=get_message("HOTELS_FOUND", replace={"COUNT": index}, lang=userdata.language))


def route(user_id: int, chat_id: int | str):
    """
    В зависимости от выбранной на первом шаге команды выполняем запрос к сервису и выводим результат
    """

    userdata = ParametersManager().get_parameters(user_id)
    if userdata.command == "/highprice":
        result = rapid.highprice(userdata)
        print_results(user_id, chat_id, result)
    elif userdata.command == "/lowprice":
        result = rapid.lowprice(userdata)
        print_results(user_id, chat_id, result)
    else:
        result = rapid.bestdeal(userdata)
        print_results(user_id, chat_id, result)

