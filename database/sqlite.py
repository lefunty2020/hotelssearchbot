from peewee import *
import datetime

db = SqliteDatabase("hotelsbot_v2.db")


class BaseModel(Model):
    class Meta:
        database = db


class History(BaseModel):
    """
    История команд с параметрами
    """
    chat_id = CharField()
    user_id = CharField()
    record_date = DateTimeField(default=datetime.datetime.now)
    command = CharField()
    parameters = CharField()


class Messages(BaseModel):
    """
    История полученных сообщений
    """
    history = ForeignKeyField(History, backref='messages')
    record_date = DateTimeField(default=datetime.datetime.now)
    caption = CharField()


class Photos(BaseModel):
    """
    Фотографии к сообщениям
    """
    message = ForeignKeyField(Messages, backref='photos')
    record_date = DateTimeField(default=datetime.datetime.now)
    data = BlobField()


class Options(BaseModel):
    """
    Настройки бота для конкретного пользователя
    """
    user_id = CharField()
    option_id = CharField()
    option_value = CharField()





