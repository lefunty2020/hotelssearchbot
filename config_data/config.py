import os
from dotenv import load_dotenv, find_dotenv
from lang.labels import get_message

if not find_dotenv():
    exit(get_message('ERROR_ENV_NOTFOUND'))
else:
    load_dotenv()

BOT_TOKEN = os.getenv('BOT_TOKEN')
RAPID_API_KEY = os.getenv('RAPID_API_KEY')
DEFAULT_COMMANDS = (
    ('start', get_message('COMMAND_START')),
    ('lowprice', get_message('COMMAND_LOWPRICE')),
    ('highprice', get_message('COMMAND_HIGHPRICE')),
    ('bestdeal', get_message('COMMAND_BESTDEAL')),
    ('history', get_message('COMMAND_HISTORY')),
    ('options', get_message('COMMAND_OPTIONS')),
    ('help', get_message('COMMAND_HELP'))
)

DEFAULT_LANGUAGE = "ru"
DEFAULT_LANGUAGE_API = "ru_RU"
DEFAULT_CURRENCY = "RUB"
DEFAULT_DATETIME_FORMAT = "%d.%m.%Y %H:%M:%S"
DEFAULT_DATE_FORMAT = "%d.%m.%Y"
DEFAULT_TIME_FORMAT = "%H:%M:%S"
